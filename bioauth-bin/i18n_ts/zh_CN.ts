<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>QObject</name>
    <message>
        <location filename="../src/main.cpp" line="99"/>
        <source>Try it again</source>
        <translation>再试一次</translation>
    </message>
    <message>
        <location filename="../src/main.cpp" line="101"/>
        <source>Use other Devices</source>
        <translation>使用其他设备</translation>
    </message>
    <message>
        <location filename="../src/main.cpp" line="102"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../src/main.cpp" line="111"/>
        <source>Choose the option</source>
        <translation>选择选项</translation>
    </message>
    <message>
        <location filename="../src/main.cpp" line="133"/>
        <location filename="../src/main.cpp" line="168"/>
        <location filename="../src/main.cpp" line="177"/>
        <source>Invaild response &quot;</source>
        <translation>无效的响应 &quot;</translation>
    </message>
    <message>
        <location filename="../src/main.cpp" line="134"/>
        <location filename="../src/main.cpp" line="169"/>
        <location filename="../src/main.cpp" line="178"/>
        <location filename="../src/main.cpp" line="287"/>
        <source>AUTHENTICATION CANCELED</source>
        <translation>认证被取消</translation>
    </message>
    <message>
        <location filename="../src/main.cpp" line="145"/>
        <source>=== BIOMETRIC DEVICES ===</source>
        <translation>=== 生物识别设备 ===</translation>
    </message>
    <message>
        <location filename="../src/main.cpp" line="156"/>
        <source>Choose the device</source>
        <translation>选择设备</translation>
    </message>
    <message>
        <location filename="../src/main.cpp" line="200"/>
        <source>Sevice Name</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/main.cpp" line="201"/>
        <source>DISPLAY env</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/main.cpp" line="202"/>
        <source>User Name</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/main.cpp" line="203"/>
        <source>Display debug information</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/main.cpp" line="241"/>
        <source>BIOMETRIC AUTHENTICATION</source>
        <translation>生物识别认证</translation>
    </message>
    <message>
        <location filename="../src/main.cpp" line="242"/>
        <source>Press Q or Esc to cancel</source>
        <translation>按Q或者Esc取消</translation>
    </message>
    <message>
        <location filename="../src/main.cpp" line="254"/>
        <source>AUTHENTICATION SUCCESS</source>
        <translation>认证成功</translation>
    </message>
    <message>
        <location filename="../src/main.cpp" line="258"/>
        <source>AUTHENTICATION FAILED</source>
        <translation>认证失败</translation>
    </message>
    <message>
        <location filename="../src/main.cpp" line="275"/>
        <source>BIOMETRIC AUTHENTICATION END</source>
        <translation>生物识别认证结束</translation>
    </message>
</context>
</TS>
