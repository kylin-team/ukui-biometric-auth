<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../src/mainwindow.cpp" line="40"/>
        <source>Authentication</source>
        <translation>授权</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="26"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <source>More</source>
        <translation type="vanished">更多</translation>
    </message>
    <message>
        <source>Restart</source>
        <translation type="vanished">重新开始</translation>
    </message>
    <message>
        <source>Password</source>
        <translation type="vanished">密码</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="153"/>
        <source>Biometric</source>
        <translation>使用生物识别</translation>
    </message>
    <message>
        <source>DeviceType:</source>
        <translation type="vanished">设备类型：</translation>
    </message>
    <message>
        <source>Back</source>
        <translation type="vanished">返回</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="188"/>
        <source>Details</source>
        <translation>详细</translation>
    </message>
    <message>
        <source>Action Id:</source>
        <translation type="vanished">动作:</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="326"/>
        <source>Description:</source>
        <translation>描述：</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="245"/>
        <source>Polkit.subject-pid:</source>
        <translation>Polkit.subject-pid：</translation>
    </message>
    <message>
        <source>Retry</source>
        <translation type="vanished">重试</translation>
    </message>
    <message>
        <source>Device types:</source>
        <translation type="vanished">设备类型：</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="268"/>
        <source>Vendor:</source>
        <translation>发行商：</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="359"/>
        <source>Action:</source>
        <translation>动作：</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="336"/>
        <source>Polkit.caller-pid:</source>
        <translation>Polkit.caller-pid：</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="398"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="411"/>
        <source>Authenticate</source>
        <translation>授权</translation>
    </message>
    <message>
        <source>Auth</source>
        <translation type="vanished">授权</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="151"/>
        <source>in authentication, please wait...</source>
        <translation>认证中，请稍等...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="184"/>
        <source>An application is attempting to perform an action that requires privileges. Authentication is required to perform this action.</source>
        <translation>一个程序正试图执行一个需要特权的动作。要求授权以执行该动作。</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="219"/>
        <source>Password: </source>
        <translation type="unfinished">密码： </translation>
    </message>
    <message>
        <source>_Password: </source>
        <translation type="obsolete">密码： </translation>
    </message>
    <message>
        <source>_Password:</source>
        <translation type="vanished">密码：</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="236"/>
        <source>Authentication failed, please try again.</source>
        <translation>认证失败，请重试。</translation>
    </message>
    <message>
        <source>Authentication failed, please try again</source>
        <translation type="vanished">认证失败，请重试</translation>
    </message>
</context>
<context>
    <name>PolkitListener</name>
    <message>
        <location filename="../src/PolkitListener.cpp" line="58"/>
        <source>Another client is already authenticating, please try again later.</source>
        <translation>有另外一个客户端正在认证，请稍后重试。</translation>
    </message>
    <message>
        <location filename="../src/PolkitListener.cpp" line="170"/>
        <source>Authentication failure, please try again.</source>
        <translation>认证失败，请重试。</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>FingerPrint</source>
        <translation type="vanished">指纹</translation>
    </message>
    <message>
        <source>FingerVein</source>
        <translation type="vanished">指静脉</translation>
    </message>
    <message>
        <source>Iris</source>
        <translation type="vanished">虹膜</translation>
    </message>
    <message>
        <source>Face</source>
        <translation type="vanished">人脸</translation>
    </message>
    <message>
        <source>VoicePrint</source>
        <translation type="vanished">声纹</translation>
    </message>
</context>
</TS>
