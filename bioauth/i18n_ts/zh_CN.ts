<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>BioAuthWidget</name>
    <message>
        <location filename="../src/bioauthwidget.ui" line="14"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/bioauthwidget.ui" line="49"/>
        <location filename="../src/bioauthwidget.ui" line="90"/>
        <source>TextLabel</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/bioauthwidget.ui" line="127"/>
        <source>More</source>
        <translation>更多</translation>
    </message>
    <message>
        <location filename="../src/bioauthwidget.ui" line="140"/>
        <source>Retry</source>
        <translation>重试</translation>
    </message>
    <message>
        <location filename="../src/bioauthwidget.ui" line="153"/>
        <source>Password</source>
        <translation>密码</translation>
    </message>
</context>
<context>
    <name>BioDevices</name>
    <message>
        <location filename="../src/biodevices.cpp" line="161"/>
        <location filename="../src/biodevices.cpp" line="163"/>
        <source>FingerPrint</source>
        <translation>指纹</translation>
    </message>
    <message>
        <location filename="../src/biodevices.cpp" line="165"/>
        <source>FingerVein</source>
        <translation>指静脉</translation>
    </message>
    <message>
        <location filename="../src/biodevices.cpp" line="167"/>
        <source>Iris</source>
        <translation>虹膜</translation>
    </message>
    <message>
        <location filename="../src/biodevices.cpp" line="169"/>
        <source>Face</source>
        <translation>人脸</translation>
    </message>
    <message>
        <location filename="../src/biodevices.cpp" line="171"/>
        <source>VoicePrint</source>
        <translation>声纹</translation>
    </message>
</context>
<context>
    <name>BioDevicesWidget</name>
    <message>
        <location filename="../src/biodeviceswidget.ui" line="14"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/biodeviceswidget.ui" line="24"/>
        <source>Device types:</source>
        <translation>设备类型：</translation>
    </message>
    <message>
        <location filename="../src/biodeviceswidget.ui" line="80"/>
        <source>Back</source>
        <translation>返回</translation>
    </message>
    <message>
        <location filename="../src/biodeviceswidget.ui" line="115"/>
        <source>OK</source>
        <translation>确定</translation>
    </message>
    <message>
        <location filename="../src/biodeviceswidget.cpp" line="45"/>
        <source>FingerPrint</source>
        <translation>指纹</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>FingerPrint</source>
        <translation type="vanished">指纹</translation>
    </message>
    <message>
        <source>FingerVein</source>
        <translation type="vanished">指静脉</translation>
    </message>
    <message>
        <source>Iris</source>
        <translation type="vanished">虹膜</translation>
    </message>
    <message>
        <source>Face</source>
        <translation type="vanished">人脸</translation>
    </message>
    <message>
        <source>VoicePrint</source>
        <translation type="vanished">声纹</translation>
    </message>
</context>
</TS>
